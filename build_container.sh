#!/bin/bash

set -eEx

function cleanup_container() {
    echo "Removing working container"
    podman stop $1
    podman rm $1
}

alpine_ctr=$(buildah from "${1:-alpine}")

trap "cleanup_container $alpine_ctr" ERR

# update and upgrade
buildah run "$alpine_ctr" -- apk update
buildah run "$alpine_ctr" -- apk upgrade
# install build tools
buildah run "$alpine_ctr" -- apk add alpine-sdk build-base apk-tools alpine-conf busybox fakeroot git syslinux xorriso squashfs-tools sudo mtools dosfstools grub-efi
# add build user
# buildah run "$alpine_ctr" -- adduser build -D -G abuild
# gen keys
buildah run "$alpine_ctr" -- abuild-keygen -i -a -n
# clone aports fork
buildah run "$alpine_ctr" -- git clone https://gitlab.alpinelinux.org/alpine/aports.git /root/aports
buildah run --workingdir /root/aports "$alpine_ctr" -- git checkout -b 3.17 origin/3.17-stable
# set default user to build
# buildah config -u build "$alpine_ctr"
# set default dir
buildah copy --chmod 770 "$alpine_ctr" './build_iso.sh' '/root'
buildah copy --chmod 770 "$alpine_ctr" './mkimg.btrfs_install.sh' '/root/aports/scripts/'
buildah copy --chmod 770 "$alpine_ctr" './genapkovl-mkimgoverlay.sh' '/root/aports/scripts/'
buildah config --workingdir /root "$alpine_ctr"
# set needed volumes. one for iso and one to mount aports in.
#buildah config -v /root/aports "$alpine_ctr"
buildah config -v /root/iso "$alpine_ctr"
# tag it
buildah commit "$alpine_ctr" "${2:-$USER/alpine_build}"
# rm intermediate container
buildah rm "$alpine_ctr"
