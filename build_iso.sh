#!/bin/sh

export PROFILENAME=btrfs_install

sh aports/scripts/mkimage.sh --tag 3.17 \
	--outdir /iso \
	--arch x86_64 \
	--repository http://dl-cdn.alpinelinux.org/alpine/3.17/main \
	--profile $PROFILENAME
